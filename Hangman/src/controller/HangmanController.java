package controller;

import apptemplate.AppTemplate;
import data.GameData;
import gui.Workspace;
import javafx.animation.AnimationTimer;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.control.TextInputControl;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.HBox;
import javafx.scene.text.Text;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import propertymanager.PropertyManager;
import ui.AppInputDialog;
import ui.AppMessageDialogSingleton;
import ui.OkayButtonDialog;
import ui.YesNoCancelDialogSingleton;
import java.io.File;
import java.util.*;

import java.io.FileWriter;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.node.ObjectNode;

import javax.swing.*;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URL;
import java.nio.file.Path;

import static settings.AppPropertyType.*;
import static settings.InitializationParameters.APP_WORKDIR_PATH;
import static settings.InitializationParameters.OKAY_BUTTON;

/**
 * @author Ritwik Banerjee
 */
public class HangmanController implements FileController {

    private AppTemplate appTemplate; // shared reference to the application
    private GameData    gamedata;    // shared reference to the game being played, loaded or saved
    private Text[]      progress;    // reference to the text area for the word
    private boolean     success;     // whether or not player was successful
    private int         discovered;  // the number of letters already discovered
    private Button      gameButton;  // shared reference to the "start game" button
    private Label       remains;     // dynamically updated label that indicates the number of remaining guesses
    private boolean     gameover;    // whether or not the current game is already over
    private boolean     savable;
    private Path        workFile;
    private Workspace   workspace;
    private OkayButtonDialog dialog;

    public Path getWorkFile() {
        return workFile;
    }
    public HangmanController(AppTemplate appTemplate, Button gameButton) {
        this(appTemplate);
        this.gameButton = gameButton;
    }

    public HangmanController(AppTemplate appTemplate) {
        this.appTemplate = appTemplate;
    }

    public void enableGameButton() {
        if (gameButton == null) {
            workspace = (Workspace) appTemplate.getWorkspaceComponent();
            gameButton = workspace.getStartGame();
        }
        //gameButton.setDisable(false);
    }

    public void start() {
        gamedata = new GameData(appTemplate);
        gameover = false;
        success = false;
        savable = true;
        discovered = 0;
        workspace = (Workspace) appTemplate.getWorkspaceComponent();


        appTemplate.getGUI().updateWorkspaceToolbar(savable);//when the game is initialized we update the toolbar to allow the user to save game state.
        HBox remainingGuessBox = workspace.getRemainingGuessBox();
        HBox guessedLetters    = (HBox) workspace.getGameTextsPane().getChildren().get(1);

        remains = new Label(Integer.toString(GameData.TOTAL_NUMBER_OF_GUESSES_ALLOWED));
        remainingGuessBox.getChildren().addAll(new Label("Remaining Guesses: "), remains);
        initWordGraphics(guessedLetters);

        play();
    }

    private void end() {
        if(dialog == null) {
            dialog = OkayButtonDialog.getSingleton();
            dialog.init(appTemplate.getGUI().getWindow());
        }
        PropertyManager propertyManager = PropertyManager.getManager();

        if(success){
            dialog.show(propertyManager.getPropertyValue(WIN_GAME_TITLE), propertyManager.getPropertyValue(WIN_GAME_MESSAGE));
        }
        else{
            dialog.show(propertyManager.getPropertyValue(LOOSE_GAME_TITLE), propertyManager.getPropertyValue(LOOSE_GAME_MESSAGE) + " The word was: " + "\"" + gamedata.getTargetWord() +  "\"" + ".");
        }
        appTemplate.getGUI().getPrimaryScene().setOnKeyTyped(null);
        gameover = true;
        gameButton.setDisable(false);
        savable = false; // cannot save a game that is already over
        appTemplate.getGUI().updateWorkspaceToolbar(savable);
        appTemplate.getDataComponent().reset();                // reset the data (should be reflected in GUI)
        ensureActivatedWorkspace();                            // ensure workspace is activated
        workspace.reinitialize();
        appTemplate.getWorkspaceComponent().reloadWorkspace(); // load data into workspace
    }

    private void initWordGraphics(HBox guessedLetters) {
        char[] targetword = gamedata.getTargetWord().toCharArray();
        progress = new Text[targetword.length];
        for (int i = 0; i < progress.length; i++) {
            progress[i] = new Text(Character.toString(targetword[i]));
            progress[i].setVisible(false);
        }
        guessedLetters.getChildren().addAll(progress);
    }

    public void play() {
        AnimationTimer timer = new AnimationTimer() {
            @Override
            public void handle(long now) {
                appTemplate.getGUI().getPrimaryScene().setOnKeyTyped((KeyEvent event) -> {
                    char guess = event.getCharacter().charAt(0);
                    if (!alreadyGuessed(guess)) {
                        boolean goodguess = false;
                        for (int i = 0; i < progress.length; i++) {
                            if (gamedata.getTargetWord().charAt(i) == guess) {
                                progress[i].setVisible(true);
                                gamedata.addGoodGuess(guess);
                                goodguess = true;
                                discovered++;
                            }
                        }
                        if (!goodguess)
                            gamedata.addBadGuess(guess);

                        success = (discovered == progress.length);
                        remains.setText(Integer.toString(gamedata.getRemainingGuesses()));
                    }
                });
                gameButton.setDisable(true);

                if (gamedata.getRemainingGuesses() <= 0 || success) {
                    gameButton.setDisable(false);
                    stop();
                }
            }

            @Override
            public void stop() {
                super.stop();
                end();
            }
        };
        timer.start();
    }

    private boolean alreadyGuessed(char c) {
        return gamedata.getGoodGuesses().contains(c) || gamedata.getBadGuesses().contains(c);
    }
    
    @Override
    public void handleNewRequest() {
        AppMessageDialogSingleton messageDialog   = AppMessageDialogSingleton.getSingleton();
        PropertyManager           propertyManager = PropertyManager.getManager();
        boolean                   makenew         = true;
        if (savable) {
            try {
                makenew = promptToSave();
                if (promptToSave()) {
                    handleSaveRequest();
                }
            } catch (IOException e) {
                messageDialog.show(propertyManager.getPropertyValue(NEW_ERROR_TITLE), propertyManager.getPropertyValue(NEW_ERROR_MESSAGE));
            }
        }

        if (makenew) {
            appTemplate.getDataComponent().reset();                // reset the data (should be reflected in GUI)
            appTemplate.getWorkspaceComponent().reloadWorkspace(); // load data into workspace
            ensureActivatedWorkspace();                            // ensure workspace is activated
            workFile = null;                                       // new workspace has never been saved to a file

            Workspace gameWorkspace = (Workspace) appTemplate.getWorkspaceComponent();
            gameWorkspace.reinitialize();
            enableGameButton();
            start();
        }

        if (gameover) {

            savable = false;
            appTemplate.getGUI().updateWorkspaceToolbar(savable);
            Workspace gameWorkspace = (Workspace) appTemplate.getWorkspaceComponent();
            gameWorkspace.reinitialize();
            enableGameButton();
        }

    }
    
    @Override
    public void handleSaveRequest() throws IOException {
        boolean userChoice= promptToSave();
        if(userChoice) {
            PropertyManager propertyManager = PropertyManager.getManager();
            try {
                if (workFile != null) {
                    save(gamedata);
                }
                else {
                    FileChooser fileChooser = new FileChooser();
                    File file = new File("Hangman/resources/" + APP_WORKDIR_PATH.getParameter());
                    String absolutePath = "File://" + file.getAbsolutePath();
                    URL         workDirURL  = new URL(absolutePath);
                    if (workDirURL == null)
                        throw new FileNotFoundException("Work folder not found under resources.");

                    File initialDir = new File(workDirURL.getPath());
                    fileChooser.setInitialDirectory(initialDir);
                    fileChooser.setTitle(propertyManager.getPropertyValue(SAVE_WORK_TITLE));
                    fileChooser.getExtensionFilters().addAll(new FileChooser.ExtensionFilter(propertyManager.getPropertyValue(WORK_FILE_EXT_DESC),
                            propertyManager.getPropertyValue(WORK_FILE_EXT)));
                    Stage window = appTemplate.getGUI().getWindow();
                    File selectedFile = fileChooser.getInitialDirectory();
                    Path selectedFilePath = selectedFile.toPath();
                    if (selectedFile != null)
                        save(gamedata);
                }
            } catch (IOException ioe) {
                AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
                dialog.show(propertyManager.getPropertyValue(SAVE_ERROR_TITLE), propertyManager.getPropertyValue(SAVE_ERROR_MESSAGE));
            }
        }
    }

    @Override
    public void handleLoadRequest() {
        try {
            FileChooser fileChooser = new FileChooser();
            File selectedFile = fileChooser.showOpenDialog(appTemplate.getGUI().getWindow());
            //File initialDir = new File("Hangman/resources/saved");
            //fileChooser.setInitialDirectory(initialDir);
            File location = selectedFile;
            ObjectMapper mapper = new ObjectMapper();
            //Map<String, Object> hangmanDataMap = new HashMap<String, Object>();
            Map<String, Object> hangmanDataMap = mapper.readValue(location, Map.class);

            ArrayList<Character> badGuessList = new ArrayList<>();
            badGuessList.addAll((Collection<? extends Character>) hangmanDataMap.get("badGuesses"));
            Set<Character> badGuessSet = new HashSet<Character>(badGuessList);

            ArrayList<Character> goodGuessList = new ArrayList<>();
            goodGuessList.addAll((Collection<? extends Character>) hangmanDataMap.get("goodGuesses"));
            Set<Character> goodGuessSet = new HashSet<Character>(goodGuessList);

            gamedata.setRemainingGuesses((Integer) hangmanDataMap.get("remainingGuesses"));
            gamedata.setTargetWord((String) hangmanDataMap.get("targetWord"));
            gamedata.setBadGuesses(badGuessSet);
            gamedata.setGoodGuesses(goodGuessSet);


            appTemplate.getDataComponent().reset();                // reset the data (should be reflected in GUI)
            //appTemplate.getWorkspaceComponent().reloadWorkspace(); // load data into workspace
            ensureActivatedWorkspace();                            // ensure workspace is activated
            workspace.reinitialize();
            //enableGameButton();

            HBox remainingGuessBox = workspace.getRemainingGuessBox();
            HBox guessedLetters    = (HBox) workspace.getGameTextsPane().getChildren().get(1);

            remains = new Label(Integer.toString(GameData.TOTAL_NUMBER_OF_GUESSES_ALLOWED));
            remainingGuessBox.getChildren().addAll(new Label("Remaining Guesses: "), remains);
            initWordGraphics(guessedLetters);

            String goodGuesses = "";
            Object[] goodGuessArray = gamedata.getGoodGuesses().toArray();
            for(int i = 0; i<goodGuessArray.length; i++){
                goodGuesses += goodGuessArray[i];
            }
            for (int i = 0; i < goodGuesses.length(); i++) {
                char guess = goodGuesses.charAt(i);
                for (int j = 0; j < progress.length; j++) {
                    if (gamedata.getTargetWord().charAt(j) == guess) {
                        progress[j].setVisible(true);
                        discovered++;
                    }
                }
            }
            discovered = gamedata.getGoodGuesses().size();
            remains.setText(Integer.toString(gamedata.getRemainingGuesses()));
            play();
        }catch (JsonParseException e) { e.printStackTrace();}
        catch (JsonMappingException e) { e.printStackTrace();}
        catch (IOException e) { e.printStackTrace();}
    }
    @Override
    public void handleExitRequest() {
        try {
            boolean exit = true;
            if (savable)
                promptToSave();
            if (exit)
                System.exit(0);
        } catch (IOException ioe) {
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
            PropertyManager           props  = PropertyManager.getManager();
            dialog.show(props.getPropertyValue(SAVE_ERROR_TITLE), props.getPropertyValue(SAVE_ERROR_MESSAGE));
        }
    }

    private void ensureActivatedWorkspace() {
        appTemplate.getWorkspaceComponent().activateWorkspace(appTemplate.getGUI().getAppPane());
    }
    //EDIT THIS METHOD
    private boolean promptToSave() throws IOException {
        /*PropertyManager            propertyManager   = PropertyManager.getManager();
        YesNoCancelDialogSingleton yesNoCancelDialog = YesNoCancelDialogSingleton.getSingleton();

        yesNoCancelDialog.show(propertyManager.getPropertyValue(SAVE_UNSAVED_WORK_TITLE),
                propertyManager.getPropertyValue(SAVE_UNSAVED_WORK_MESSAGE));
        return false;*/
        PropertyManager            propertyManager   = PropertyManager.getManager();
        YesNoCancelDialogSingleton yesNoCancelDialog = YesNoCancelDialogSingleton.getSingleton();

        yesNoCancelDialog.show(propertyManager.getPropertyValue(SAVE_UNSAVED_WORK_TITLE),
                propertyManager.getPropertyValue(SAVE_UNSAVED_WORK_MESSAGE));

        if (yesNoCancelDialog.getSelection().equals(YesNoCancelDialogSingleton.YES)) {
            return true;
        }

        return false;

    }

    /**
     * A helper method to save work. It saves the work, marks the current work file as saved, notifies the user, and
     * updates the appropriate controls in the user interface
     *
     * @param target The file to which the work will be saved.
     * @throws IOException
     */

    private void save(GameData target) throws IOException { //locate the variables which we are storing
        writeJSON(target);
    }
    private void writeJSON(GameData data) throws JsonGenerationException, JsonMappingException, IOException{
        ObjectMapper mapper = new ObjectMapper();
        //Gather the data to be saved
        Set<Character> badGuesses = gamedata.getBadGuesses();
        Set<Character> goodGuesses = gamedata.getGoodGuesses();
        int remainingGuesses = gamedata.getRemainingGuesses();
        String targetWord = gamedata.getTargetWord();

        Map<String,Object> hangmanDataMap = new HashMap<String,Object>();
        hangmanDataMap.put("badGuesses", badGuesses);
        hangmanDataMap.put("goodGuesses", goodGuesses);
        hangmanDataMap.put("remainingGuesses", remainingGuesses);
        hangmanDataMap.put("targetWord", targetWord);
        Path workPath;
        if(workFile == null) {
            AppInputDialog inputDialog = new AppInputDialog(appTemplate.getGUI().getWindow());
            String userInput = inputDialog.getResult();
            File location = new File("Hangman/resources/saved/" + userInput + ".json");
            workPath = location.toPath();
            workFile = workPath;
        }
        mapper.writeValue(workFile.toFile(), hangmanDataMap);
    }
}
